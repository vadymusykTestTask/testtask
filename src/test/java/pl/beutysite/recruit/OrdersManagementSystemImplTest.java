package pl.beutysite.recruit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.beutysite.recruit.dont_touch.ItemsRepository;
import pl.beutysite.recruit.dont_touch.TaxOfficeAdapter;
import pl.beutysite.recruit.impl.OrdersManagementSystemImpl;
import pl.beutysite.recruit.orders.Order;
import pl.beutysite.recruit.orders.OrderFlag;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrdersManagementSystemImplTest {

    @Mock
    private TaxOfficeAdapter taxOfficeAdapter;

    @Mock
    private ItemsRepository itemsRepository;

    @InjectMocks
    private OrdersManagementSystemImpl ordersManagementSystem = new OrdersManagementSystemImpl(taxOfficeAdapter, itemsRepository);

    @Before
    public void setUp() throws Exception {
        when(itemsRepository.fetchItemPrice(1)).thenReturn(new BigDecimal("3.33"));
        when(itemsRepository.fetchItemPrice(2)).thenReturn(new BigDecimal("10.00"));

    }

    @Test
    public void priority_order_should_be_returned_first() {

        ordersManagementSystem.createOrder(1, 1, OrderFlag.STANDARD);
        ordersManagementSystem.createOrder(2, 1, OrderFlag.PRIORITY);
        Order nextOrder = ordersManagementSystem.fetchNextOrder();

        assertThat(nextOrder).isNotNull();
        assertThat(nextOrder.getItemId()).isEqualTo(2);

    }

    @Test
    public void tax_amount_sent_to_tax_office_should_be_correct() {

        ordersManagementSystem.createOrder(1, 1, OrderFlag.PRIORITY);
        Order nextOrder = ordersManagementSystem.fetchNextOrder();

        //then
        assertThat(nextOrder).isNotNull();

        //should be 0.77 tax because:
        // 3.33 + 1.5% = 3.38   3.38 * 23.5% = 0.80
        verify(taxOfficeAdapter).registerTax(new BigDecimal("0.80"));
    }

    @Test
    public void createNewOrderShouldIncludeOrderWithEnumProperties() {
        //TODO: Test makes me sure, that we correctly get properties from enum
        ordersManagementSystem.createOrder(2, 3, OrderFlag.PRIORITY);
        Order getOrder = ordersManagementSystem.fetchNextOrder();
        assertTrue(getOrder.getOrderFlag().getPercentage() == 1.5);
        assertEquals(getOrder.getOrderFlag().getEventActions().get(1),
                "*** This is priority order, hurry up! ***");
    }

}