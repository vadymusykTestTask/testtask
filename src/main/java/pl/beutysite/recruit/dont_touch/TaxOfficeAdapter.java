package pl.beutysite.recruit.dont_touch;

import java.math.BigDecimal;

public interface TaxOfficeAdapter {
    void registerTax(BigDecimal amount);
}
