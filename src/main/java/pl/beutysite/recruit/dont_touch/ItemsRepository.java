package pl.beutysite.recruit.dont_touch;

import java.math.BigDecimal;

public interface ItemsRepository {

    BigDecimal fetchItemPrice(int itemId);
}
