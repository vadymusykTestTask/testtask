package pl.beutysite.recruit.dont_touch;

import pl.beutysite.recruit.orders.OrderFlag;
import pl.beutysite.recruit.orders.Order;

public interface OrdersManagementSystem {

    void createOrder(int itemId, int customerId, OrderFlag... flags);

    Order fetchNextOrder();
}
