package pl.beutysite.recruit.dont_touch;

public interface SeriousEnterpriseEventBus {
    void sendEvent(String event);
}
