package pl.beutysite.recruit.impl;

import pl.beutysite.recruit.dont_touch.ItemsRepository;
import pl.beutysite.recruit.dont_touch.OrdersManagementSystem;
import pl.beutysite.recruit.dont_touch.TaxOfficeAdapter;
import pl.beutysite.recruit.orders.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class OrdersManagementSystemImpl implements OrdersManagementSystem {

    //external systems
    private final TaxOfficeAdapter taxOfficeAdapter;

    private final ItemsRepository itemsRepository;


    public OrdersManagementSystemImpl(TaxOfficeAdapter taxOfficeAdapter, ItemsRepository itemsRepository) {
        this.taxOfficeAdapter = taxOfficeAdapter;
        this.itemsRepository = itemsRepository;
    }

    private Set<Order> ordersQueue = new HashSet<>();

    private Order newOrder = null;

    @Override
    public void createOrder(int itemId, int customerId, OrderFlag... flags) {

        //fetch price and calculate percentage and taxes
        BigDecimal itemPrice = itemsRepository.fetchItemPrice(itemId);

        //create and queue order
        OrderFlag flag = flags[0];
        newOrder = new Order(itemId, customerId, itemPrice, flags[0]);

        ordersQueue.add(newOrder);

        //JIRA-18883 Fix priority orders not always being fetched first

        ///I cant't catch this bug, maybe you can tell me, how to imitate data to see it?
        if (OrderFlag.PRIORITY.equals(flag)) {
            while (fetchNextOrder() != newOrder) {
                ordersQueue.remove(newOrder);
                newOrder = new Order(itemId, customerId, itemPrice, flag);
                ordersQueue.add(newOrder);
            }
            ordersQueue.add(newOrder);
        }

        //send tax due amount
        BigDecimal tax = newOrder.getTax();
        taxOfficeAdapter.registerTax(tax);
    }

    @Override
    public Order fetchNextOrder() {
        return ordersQueue.iterator().next();
    }
}
