package pl.beutysite.recruit.helper;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class TaxCalculationsHelper {

    private final static int CURRENCY_SCALE = 3; //TODO: was mistaken to round to 2 points during calculation,

    private final static MathContext mathContext = new MathContext(CURRENCY_SCALE, RoundingMode.UP);

    public static BigDecimal getPercentagePart(BigDecimal base, BigDecimal percentage) {
        BigDecimal percentagePart = percentage.divide(new BigDecimal(100), mathContext).multiply(base, mathContext);
        // TODO: but the result must  rounded to the hundredths
        return percentagePart.setScale(2, RoundingMode.UP);
    }

    public static BigDecimal addPercentage(BigDecimal base, BigDecimal percentage) {
        BigDecimal sumResult = base.add(getPercentagePart(base, percentage), mathContext);
        return sumResult.setScale(2, RoundingMode.UP);
    }

    public static BigDecimal subtractPercentage(BigDecimal base, BigDecimal percentage) {
        BigDecimal decimalResult = base.subtract(getPercentagePart(base, percentage), mathContext);
        return decimalResult.setScale(2, RoundingMode.UP);
    }
}
