package pl.beutysite.recruit.orders;

import lombok.*;

import java.math.BigDecimal;
import java.util.Random;

@Getter
public class Order {

    private int itemId;
    private int customerId;
    private BigDecimal price;
    private OrderFlag orderFlag;

    //for performance reasons lets pre-calculate it in constructor  //TODO: as you wish :D
    private int preCalculatedHashCode = 0;

    private static Random random = new Random();

    public Order(int itemId, int customerId, BigDecimal price, OrderFlag orderFlag) {
        this.itemId = itemId;
        this.customerId = customerId;
        this.price = price;
        this.orderFlag = orderFlag;
        preCalculatedHashCode = random.nextInt();
    }

    public void process() {
        orderFlag.sendEvents();
    }

    public BigDecimal getTotalAmount() {
        return price.add(getTax());
    }


    public BigDecimal getPrice() {

        return orderFlag.calculatePrice(price);
    }

    public BigDecimal getTax() {
        return orderFlag.calculateTax(getPrice());
    }

    @Override
    public int hashCode() {
        return preCalculatedHashCode;
    }

}
