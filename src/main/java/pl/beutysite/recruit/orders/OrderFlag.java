package pl.beutysite.recruit.orders;

import lombok.Getter;
import pl.beutysite.recruit.dont_touch.SeriousEnterpriseEventBusLookup;
import pl.beutysite.recruit.helper.TaxCalculationsHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public enum OrderFlag {
    //TODO: here we can use property file to load different enum values and for quick property changes
    PRIORITY(1.5, 23.5, new ArrayList<>(Stream.of("Order processing started",
            "*** This is priority order, hurry up! ***", "Initiate shipment",
            "Order processing finished").collect(Collectors.toList()))),
    DISCOUNTED(11.0, 23.5, new ArrayList<>(Stream.of("Order processing started",
            "Run fraud detection and revenue integrity check", "Order processing finished",
            "Order processing finished").collect(Collectors.toList()))),
    INTERNATIONAL(0, 11, new ArrayList<>(Stream.of("Order processing started",
            "Dispatch translated order confirmation email", "Initiate shipment",
            "Order processing finished").collect(Collectors.toList()))),
    STANDARD(0, 23.5, new ArrayList<>(Stream.of("Order processing started", "Initiate shipment",
            "Order processing finished").collect(Collectors.toList())));

    protected double percentage;

    protected double tax;

    private ArrayList<String> eventActions = new ArrayList<>();

    OrderFlag(double percentage, double tax, ArrayList<String> eventActions) {
        this.percentage = percentage;
        this.tax = tax;
        this.eventActions = eventActions;
    }

    public BigDecimal calculatePrice(BigDecimal price) {
        if (this == DISCOUNTED) {
            return TaxCalculationsHelper.subtractPercentage(price, new BigDecimal(percentage));
        } else if (this == PRIORITY) {
            return TaxCalculationsHelper.addPercentage(price, new BigDecimal(percentage));
        } else return price;

    }

    public BigDecimal calculateTax(BigDecimal price) {
        return TaxCalculationsHelper.getPercentagePart(price, new BigDecimal(tax));
        //calculating standard tax - 23.5%  //TODO: Now this method calculates tax due enum property
    }

    public void sendEvents() {
        this.getEventActions().forEach(SeriousEnterpriseEventBusLookup.seeb::sendEvent);
    }
}
